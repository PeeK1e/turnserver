#!/bin/bash
echo $(pwd)
echo "running as $(whoami)"
turnserver -a \
            -v \
            -p "${TURN_PORT}" \
            -L "${TURN_LISTEN_ADDRESS}" \
            --server-name "${TURN_SERVER_NAME}" \
            --static-auth-secret="${TURN_SECRET}" \
            --realm="${TURN_REALM}" \
            --min-port "${TURN_PORT_START}" \
            --max-port "${TURN_PORT_END}"

