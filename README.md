## Turnserver in Docker

This is a simple Turnserver in a Docker Container.

Pull the repository set the environment variables in the compose yaml to your needs and run with `docker-compose up -d`